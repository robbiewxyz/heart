const PROJECT_NAME = `%PROJECT_NAME%`
const SPREADSHEET_ID = `%SPREADSHEET_ID%`
const GAPI_INIT = {
	clientId: `%CLIENT_ID%`,
	apiKey: `%API_KEY%`,
	scope: `https://www.googleapis.com/auth/spreadsheets`,
	discoveryDocs: [ `https://sheets.googleapis.com/$discovery/rest?version=v4` ],
}
// sheet key, sheet proper name, number of heading rows
const SHEETS = [
	[ `memberships`, `Memberships`, 2 ],
	[ `todo`,        `To do`,       2 ],
	[ `checkins`,    `Checkins`,    2 ],
]
// row key, row heading, data type
const HEADINGS = [
	[ `person`,  `PERSON`,  `id`     ],
	[ `name`,    `NAME`,    `text`   ],
	[ `phone`,   `PHONE`,   `phone`  ],
	[ `plan`,    `PLAN`,    `text`   ],
	[ `price`,   `PRICE`,   `number` ],
	[ `start`,   `START`,   `date`   ],
	[ `end`,     `END`,     `date`   ],
	[ `note`,    `NOTE`,    `text`   ],
	[ `type`,    `TYPE`,    `text`   ],
	[ `months`,  `MONTHS`,  `number` ],
	[ `date`,    `DATE`,    `date`   ],
	[ `time`,    `TIME`,    `time`   ],
	[ `todo`,    `TODO`,    `text`   ],
]
// sort priority for spreadsheet data
const SORT_BY = [ `start`, `date`, `time` ]
// number of memberships on a page
const PAGE_SIZE = 10

const { createStore, combineReducers, applyMiddleware } = Redux
const { createElement: h, Fragment, memo, useState, useReducer, useMemo, useEffect, useCallback, useRef } = React
const { render, createPortal } = ReactDOM
const { Provider, useSelector, shallowEqual, useDispatch } = ReactRedux
const useShallowSelector = selector => useSelector (selector, shallowEqual)

const store = createStore (combineReducers ({
	loaded: (loaded = {
		local: null,
		gapi: null,
		auth2: null,
		gapiInit: null,
		spreadsheet: null,
	}, action) => { switch (action.type) {
		case `LOADED`: return { ...loaded, [action.loader]: true }
		case `LOAD_FAILED`: return { ...loaded, [action.loader]: false }
		case `LOAD_RETRY`: return { ...loaded, [action.loader]: null }
		default: return loaded
	} },
	signedIn: (signedIn = null, action) => { switch (action.type) {
		case `LOADED`: return action.payload.signedIn || signedIn
		case `SIGNIN`: return action.signedIn
		default: return signedIn
	} },
	syncQueue: (syncQueue = [], action) => { switch (action.type) {
		case `LOADED`: return action.payload.syncQueue || syncQueue
		case `SYNCED`: return syncQueue.filter (aa => aa !== action.action)
		case `APPEND`: return [ ...syncQueue, action ]
		default: return syncQueue
	} },
	rows: (rows = {
		memberships: [],
		todo: [],
		checkins: [],
	}, action) => { switch (action.type) {
		case `LOADED`: return action.payload.rows || rows
		case `APPEND`: return { ...rows, [action.sheet]: [ ...rows [action.sheet], { ...action.row, index: rows [action.sheet].length } ] }
		default: return rows
	} },
	keys: (keys = {
		memberships: [],
		todo: [],
		checkins: [],
	}, action) => { switch (action.type) {
		case `LOADED`: return action.payload.keys || keys
		default: return keys
	} },
	search: (search = { search: ``, count: PAGE_SIZE }, action) => { switch (action.type) {
		case `LOADED`: return { ...search, ...action.payload.search }
		case `SEARCH`: return action.search
		default: return search
	} },
	waiver: (waiver = {
		pages: [
			{ src: `./agreement1.png`, fields: [
				{ signature: true,
					get: s => s.waiver.initialOne, set: d => ref => d ({ type: `WAIVER_INITIAL_ONE`, ref }),
					bottom: 7.31, left: 6.49, width: 0.85, height: 0.16 },
				{ signature: true,
					get: s => s.waiver.initialTwo, set: d => ref => d ({ type: `WAIVER_INITIAL_TWO`, ref }),
					bottom: 5.93, left: 6.49, width: 0.85, height: 0.16 },
			] },
			{ src: `./agreement2.png`, fields: [
				{ readonly: true,
					get: () => dateFns.format (Date.now (), `Do`),
					bottom: 5.17, left: 1.27, width: 1.19, height: 0.16 },
				{ readonly: true,
					get: () => dateFns.format (Date.now (), `MMMM`),
					bottom: 5.17, left: 2.91, width: 1.34, height: 0.16 },
				{ readonly: true,
					get: () => dateFns.format (Date.now (), `YY`),
					bottom: 5.17, left: 4.49, width: 0.26, height: 0.16 },
				{ phone: true,
					get: s => s.waiver.phone, set: d => v => d ({ type: `WAIVER_PHONE`, phone: v }),
					validate: s => alphanumeric (s.waiver.phone) === `` ? `* Required` : s.rows.memberships.map (m => m.phone).find (fuzzy (s.waiver.phone)) ? `* Phone Number Already Exists` : null,
					bottom: 4.56, left: 0.99, width: 2.19, height: 0.16 },
				{ get: s => s.waiver.name, set: d => v => d ({ type: `WAIVER_NAME`, name: v }),
					validate: s => alphanumeric (s.waiver.name) === `` ? `* Required` : s.rows.memberships.map (m => m.name).find (fuzzy (s.waiver.name)) ? `* Name Already Exists` : null,
					bottom: 3.96, left: 0.99, width: 2.19, height: 0.16 },
				{ signature: true,
					get: s => s.waiver.signatureOne, set: d => ref => d ({ type: `WAIVER_SIGNATURE_ONE`, ref }),
					bottom: 3.35, left: 0.99, width: 2.19, height: 0.16 },
				{ signature: true,
					get: s => s.waiver.signatureTwo, set: d => ref => d ({ type: `WAIVER_SIGNATURE_TWO`, ref }),
					bottom: 2.75, left: 0.99, width: 2.19, height: 0.16 },
				{ print: true,
					get: s => `Plan: ${s.waiver.plan}${s.waiver.note ? ` (${s.waiver.note})` : ``}`,
					bottom: 1.50, left: 0.99, width: 3.0, height: 0.16 },
			] },
		],
		initialOne: null,
		initialTwo: null,
		phone: ``,
		name: ``,
		signatureOne: null,
		signatureTwo: null,
		plan: `member`,
		note: ``,
		modal: false,
		open: false,
	}, action) => { switch (action.type) {
		case `WAIVER_INITIAL_ONE`: return { ...waiver, initialOne: action.ref }
		case `WAIVER_INITIAL_TWO`: return { ...waiver, initialTwo: action.ref }
		case `WAIVER_PHONE`: return { ...waiver, phone: action.phone }
		case `WAIVER_NAME`: return { ...waiver, name: action.name }
		case `WAIVER_SIGNATURE_ONE`: return { ...waiver, signatureOne: action.ref }
		case `WAIVER_SIGNATURE_TWO`: return { ...waiver, signatureTwo: action.ref }
		case `WAIVER_PLAN`: return { ...waiver, plan: action.plan }
		case `WAIVER_NOTE`: return { ...waiver, note: action.note }
		case `WAIVER_OPEN`: return { ...waiver, open: true, modal: false, phone: ``, name: `` }
		case `WAIVER_CANCEL`: return { ...waiver, open: false }
		case `WAIVER_SUBMIT`: return { ...waiver, modal: true, plan: `member`, note: `` }
		case `WAIVER_ESCAPE`: return { ...waiver, modal: false }
		case `WAIVER_SAVE`: return { ...waiver, open: false }
		default: return waiver
	} },
}), {}, applyMiddleware (store => next => action => {
	const prev = store.getState ()
	try {
		return next (action)
	} finally {
		console.groupCollapsed (`action`, action.type)
		console.log ('prev state', prev)
		console.log ('action', action)
		console.log ('next state', window.state = store.getState ())
		console.groupEnd ()
	}
}))

const fuzzy = a => b => alphanumeric (a) === alphanumeric (b)
const alphanumeric = a => (a || ``).toLowerCase ().replace (/[^\w]/g, '')

const uuid = (length = 5) => {
	let uuid = ``
	for (let i = 0; i < length; i++) uuid += (~~ (Math.random () * 26)).toString (26)
	return uuid
}

const parseSheet = ({ values }, headingRows) => {
	if (values.length <= headingRows) return { rows: [], keys: [] }

	const headings = values [headingRows - 1].map (text => HEADINGS.find (heading => heading [1] === text))

	const rows = []
	for (let i = headingRows; i < values.length; i++) {
		const row = values [i]
		if (row.length === 0) continue
		rows.push (row.reduce ((rows, value, offset) => {
			const heading = headings [offset]
			if (!heading) return rows
			const [ key, column, type ] = heading
			// again, prefer the first occurrence
			return { [key]: parseValue (value, type), ...rows }
		}, { index: rows.length }))
	}
	rows.sort ((a, b) => {
		for (let i = 0; i < SORT_BY.length; i++) {
			const key = SORT_BY [i]
			if (a [key] < b [key]) return 1
			if (a [key] > b [key]) return -1
		}
		return 0
	})

	const keys = headings.map (heading => heading && heading [0])

	return { rows, keys }
}

const parseValue = (value, type) => { switch (type) {
	case `text`: return `${value}`.trim ()
	case `date`: return value ? parseDate (value) : null
	case `time`: return value ? parseTime (value) : null
	case `number`: return `${value}`.replace (`$`, ``)  * 1 || 0
	case `phone`: return `${value}`.trim ()
	case `id`: return `${value}`.trim ()
	default: return `${value}`.trim ()
} }

const lettersByColumn = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`.split (``)

const appendSheet = (sheet, row) => {
	const keys = store.getState ().keys [sheet]
	if (!keys) return Promise.reject ()
	const values = keys.map (key => {
		if (!key) return ``
		const type = HEADINGS.find (h => h [0] === key) [2]
		return formatValue (row [key], type)
	})
	console.log (`INSERT_ROWS`, sheet, values)
	return gapi.client.sheets.spreadsheets.values.append ({
		spreadsheetId: SPREADSHEET_ID,
		range: sheetToRange (SHEETS.find (s => s [0] === sheet) [1]),
		valueInputOption: `USER_ENTERED`,
		insertDataOption: `INSERT_ROWS`,
		includeValuesInResponse: true,
		resource: { values: [ values ] },
	})
}

const formatValue = (value, type) => { switch (type) {
	case `date`: return value ? formatDate (value) : ``
	case `time`: return value ? formatTime (value) : ``
	case `id`: return value ? `'${value}` : ``
	default: return value ? `${value}` : ``
} }

const formatDate = timestamp => dateFns.format (timestamp, `MM/DD/YYYY`)
const formatTime = timestamp => dateFns.format (timestamp, `h:mm:ss A`)

const parseDate = date => new Date (`12:00:00 AM ${date}`) * 1
const parseTime = time => new Date (`${time} ${formatDate (timestampToday ())}`) * 1

const timestampToday = () => parseDate (formatDate (Date.now () - 1000 * 60 * 60 * 4))
const timestampNow = () => Date.now ()

const toOrdinal = n => n + ([ , 'st', 'nd', 'rd' ] [n % 100 >> 3 ^ 1 && n % 10] || 'th')

const sheetToRange = sheet => `${sheet}!A:ZZ`

const Wrapper = () => {
	return h (Fragment, null,
		h (LocalLoader),
		h (LocalWorker),
		h (GapiLoader),
		h (Auth2Loader),
		h (GapiInitLoader),
		h (SignInListener),
		h (SyncWorker),
		h (SpreadsheetLoader),
		h (App),
	)
}

const Loader = ({ loader, ready, promise, retry = true }) => {
	const dispatch = useDispatch ()
	const loaded = useSelector (s => s.loaded [loader])
	useEffect (() => {
		if (ready === true && loaded === null) promise ().then (
			(payload = {}) => dispatch ({ type: `LOADED`, loader, payload }),
			error => dispatch ({ type: `LOAD_FAILED`, loader, error }))
		if (ready === true && loaded === false && retry) setTimeout (
			() => dispatch ({ type: `LOAD_RETRY`, loader }), 2000)
	}, [ ready, loaded ])
	return null
}

const LocalLoader = () => h (Loader, {
	loader: `local`, ready: true, retry: false,
	promise: () => Promise.resolve ({
		keys: JSON.parse (localStorage.getItem (`keys`) || `null`),
		rows: JSON.parse (localStorage.getItem (`rows`) || `null`),
		syncQueue: JSON.parse (localStorage.getItem (`syncQueue`) || `null`),
		signedIn: JSON.parse (localStorage.getItem (`signedIn`) || `null`),
		search: JSON.parse (localStorage.getItem (`search`) || `null`),
	}),
})

const LocalWorker = () => {
	const keys = useSelector (s => s.keys)
	const rows = useSelector (s => s.rows)
	const syncQueue = useSelector (s => s.syncQueue)
	const signedIn = useSelector (s => s.signedIn)
	const search = useSelector (s => s.search)
	useEffect (() => localStorage.setItem (`keys`, JSON.stringify (keys)), [ keys ])
	useEffect (() => localStorage.setItem (`rows`, JSON.stringify (rows)), [ rows ])
	useEffect (() => localStorage.setItem (`syncQueue`, JSON.stringify (syncQueue)), [ syncQueue ])
	useEffect (() => localStorage.setItem (`signedIn`, JSON.stringify (signedIn)), [ signedIn ])
	useEffect (() => localStorage.setItem (`search`, JSON.stringify (search)), [ search ])
	return null
}

const GapiLoader = () => h (Loader, {
	loader: `gapi`, ready: true,
	promise: () => new Promise ((res, rej) => {
		const script = document.createElement (`script`)
		script.src = `https://apis.google.com/js/api.js`
		script.defer = true
		script.async = true
		script.addEventListener (`load`, ev => res ())
		script.addEventListener (`readystatechange`, ev => script.readyState === `complete` && res ())
		script.addEventListener (`error`, ev => rej (ev))
		document.body.appendChild (script)
	}),
})

const Auth2Loader = () => h (Loader, {
	loader: `auth2`, ready: useSelector (s => s.loaded.gapi),
	promise: () => new Promise (res => gapi.load (`client:auth2`, res)),
})

const GapiInitLoader = () => h (Loader, {
	loader: `gapiInit`, ready: useSelector (s => s.loaded.auth2),
	promise: () => gapi.client.init (GAPI_INIT),
})

const SignInListener = () => {
	const ready = useSelector (s => s.loaded.gapiInit)
	const dispatch = useDispatch ()
	const onSignIn = useCallback (signedIn => {
		dispatch ({ type: `SIGNIN`, signedIn })
	}, [ dispatch ])
	useEffect (() => {
		if (ready === true) {
			gapi.auth2.getAuthInstance ().isSignedIn.listen (onSignIn) // listen for sign-in state changes.
			onSignIn (gapi.auth2.getAuthInstance ().isSignedIn.get ()) // handle the initial sign-in state.
		}
	}, [ ready, onSignIn ])
	return null
}

const SyncWorker = () => {
	const ready = useSelector (s => s.loaded.gapiInit && s.signedIn)
	const action = useSelector (s => s.syncQueue [0])
	const dispatch = useDispatch ()
	useEffect (() => {
		if (!ready || !action) {
			return
		} else if (action.type === `APPEND`) {
			appendSheet (action.sheet, action.row).then (response => dispatch ({ type: `SYNCED`, action }))
		}
	}, [ ready, action, dispatch ])
	return null
}

const SpreadsheetLoader = () => h (Loader, {
	loader: `spreadsheet`, ready: useSelector (s => s.loaded.gapiInit && s.signedIn && s.syncQueue.length === 0),
	promise: () => gapi.client.sheets.spreadsheets.values.batchGet ({
		spreadsheetId: SPREADSHEET_ID,
		ranges: SHEETS.map (sheet => sheet [1]).map (sheetToRange),
	}).then (({ result }) => SHEETS.map (([ key, name, headingRows ], index) => ([
		key, parseSheet (result.valueRanges [index], headingRows),
	])).reduce ((data, [ key, { rows, keys } ]) => ({
		rows: { ...data.rows, [key]: rows },
		keys: { ...data.keys, [key]: keys },
	}), { rows: {}, keys: {} })),
})

const App = () => {
	const dispatch = useDispatch ()
	const openWaiver = React.useCallback (() => dispatch ({ type: `WAIVER_OPEN` }), [ dispatch ])

	const local = useSelector (s => s.loaded.local)
	const signedIn = useSelector (s => s.signedIn)
	const signIn = useCallback (() => gapi.auth2.getAuthInstance ().signIn (), [])
	const signOut = useCallback (() => gapi.auth2.getAuthInstance ().signOut (), [])

	const waiverOpen = useSelector (s => s.waiver.open)
	useEffect (() => {
		document.documentElement.scrollTop = 0
	}, [ waiverOpen ])

	if (!local) return null

	return h (`main`, { className: `App Column` },
		h (`header`, { className: `Row` },
			h (`h1`, null, PROJECT_NAME),
			h (Text, null, h (StatusIndicator)),
			h (Button, { onClick: openWaiver }, `New member \u{2795}\u{FE0F}`),
			signedIn || h (Button, { onClick: signIn }, `Sign in \u{1F6AA}`),
			signedIn && h (Button, { onClick: signOut }, `Sign out \u{1F512}`),
		),
		h (Search),
		waiverOpen && h (Waiver),
	)
}

const StatusIndicator = () => {
	const signedIn = useSelector (s => s.signedIn)
	const loaded = useSelector (s => s.loaded)
	const syncing = useSelector (s => s.syncQueue.length)
	const t = timestampToday ()
	const total = useSelector (s => s.rows.checkins.filter (r => r.date === t).length)
	const news = useSelector (s => s.rows.checkins.filter (r => r.date === t && r.note === `NEW`).length)

	let loading = ``
	if (loaded.local === null) loading = `Loading cache`
	else if (!loaded.gapi) loading = `Loading gapi`
	else if (!loaded.auth2) loading = `Loading auth2 api`
	else if (!loaded.gapiInit) loading = `Connecting to gapi`
	else if (signedIn === null) loading = `Loading sign in`
	else if (signedIn === false) loading = `Not signed in`
	else if (!loaded.spreadsheet) loading = `Loading data`
	const info = syncing > 0 ? `Saving ${syncing} ${syncing === 1 ? `change` :`changes`}, ` : ``

	return h (Fragment, null,
		loading && `${loading}: `,
		`${info}${total} ${total === 1 ? `person` : `people`} checked in (${total - news} ${total - news === 1 ? `member` : `members`} and ${news} new)`
	)
}

const Search = () => {
	const dispatch = useDispatch ()
	const up = useSelector (s => s.search.search)
	const setUp = useCallback (up => dispatch ({ type: `SEARCH`, search: { search: up, count: PAGE_SIZE } }), [ dispatch ])
	const upRef = useRef (up)

	const [ down, setDown ] = useState (up)
	const downRef = useRef (down)

	const syncUp = useCallback (() => {
		if (upRef.current === downRef.current) return
		setUp (upRef.current = downRef.current)
	}, [ downRef, down, setUp ])

	const syncDown = useCallback (() => {
		if (downRef.current === upRef.current) return
		setDown (downRef.current = upRef.current)
	}, [ upRef, up, setDown ])

	useEffect (() => {
		upRef.current = up
		syncDown ()
	}, [ up ])

	const debounceTimeout = useRef (null)
	const debouncedSetDown = useCallback (ev => {
		setDown (downRef.current = ev.target.value)
		cancelAnimationFrame (debounceTimeout.current)
		debounceTimeout.current = requestAnimationFrame (syncUp)
	}, [ setDown, syncUp, debounceTimeout ])

	const inputRef = useRef ()
	useEffect (() => inputRef.current.focus (), [])

	return h (Fragment, null,
		h (`input`, { ref: inputRef, className: `SearchInput`, placeholder: `Enter a name or phone #`, onChange: debouncedSetDown, value: down }),
		h (Memberships),
	)
}

const Memberships = () => {
	const dispatch = useDispatch ()
	const { search, count } = useSelector (s => s.search)
	const moreMemberships = useCallback (() => {
		dispatch ({ type: `SEARCH`, search: { search, count: count + PAGE_SIZE } })
	}, [ dispatch, search, count ])

	const memberships = useSelector (s => s.rows.memberships)
	const filtered = useMemo (() => memberships.filter (membership => {
		if (membership.name && membership.name.toLowerCase ().indexOf (search.toLowerCase ()) !== -1) return true
		if (membership.phone && membership.phone.indexOf (search) !== -1) return true
		return false
	}), [ memberships, search ])
	const limited = useMemo (() => filtered.slice (0, count), [ filtered, count ])
	const hasMore = filtered.length > count

	return h (`div`, { className: `List Memberships` },
		limited.map (({ index }) => h (Membership, { key: index, index })),
		hasMore && h (Button, { onClick: moreMemberships }, `More`),
	)
}

const Membership = memo (({ index }) => {
	const dispatch = useDispatch ()
	const membership = useSelector (s => s.rows.memberships.find (m => m.index === index))
	const latest = useSelector (s => {
		for (let i = 0; i < s.rows.memberships.length; i++) {
			const m = s.rows.memberships [i]
			if (m === membership) return true
			if (m.person === membership.person) return false
		}
	})
	const expired = membership.end && membership.end < timestampToday ()
	const checkInMember = useCallback (() => {
		dispatch ({ type: `APPEND`, sheet: `checkins`, row: { person: membership.person, date: timestampToday (), time: timestampNow (), note: `MEMBER` } })
	}, [ membership ])
	const renewMembership = useCallback (({ type, note }) => {
		dispatch ({ type: `APPEND`, sheet: `todo`, row: { date: timestampToday (), time: timestampNow (), person: membership.person, name: membership.name, phone: membership.phone, todo: `RENEW MEMBERSHIP as ${type}${note ? ` (${note})` : ``}` } })

		dispatch ({ type: `APPEND`, sheet: `checkins`, row: { person: membership.person, date: timestampToday (), time: timestampNow (), note: type === `member` ? `MEMBER` : `GUEST` } })
		hideRenew ()
	}, [ membership ])
	const hostNote = useCallback (() => {
		const note = prompt (`Leave a note for the host`)
		if (!note) return
		dispatch ({ type: `APPEND`, sheet: `todo`, row: { date: timestampToday (), time: timestampNow (), person: membership.person, name: membership.name, phone: membership.phone, todo: `NOTE: ${note}` } })
	}, [ membership ])

	const checkIns = useShallowSelector (s => s.rows.checkins.filter (r => r.person === membership.person))
	const t = timestampToday ()
	const checkedIn = !!checkIns.find (r => r.date === t)
	const loyalty = useMemo (() => {
		const total = checkIns.length
		const news = checkIns.filter (r => r.note === `NEW`).length
		return total - news
	}, [ checkIns ])

	const [ renewShow, setRenewShow ] = useState (false)
	const showRenew = useCallback (() => setRenewShow (true), [ setRenewShow ])
	const hideRenew = useCallback (() => setRenewShow (false), [ setRenewShow ])

	return h (`article`, { className: `Membership Row${expired ? ` expired` : ``}` },
		/*h (TextCell, { className: `Person` }, membership.person),*/
		h (TextCell, { className: `Name` }, membership.name),
		h (TextCell, { className: `Phone` }, membership.phone),
		h (TextCell, { className: `Plan` }, `${membership.plan}, from ${formatDate (membership.start)}${membership.end ? ` to ${formatDate (membership.end)}` : ``}`),
		h (TextCell, { className: `End` }, ),
		h (TextCell, { className: `Note Spacer` }, membership.note),
		latest && h (Fragment, null,
			h (TextCell, { className: `Loyalty` }, `${loyalty}x`),
			checkedIn || expired || h (Button, { onClick: checkInMember }, `Check in \u{1F920}`),
			checkedIn || expired && h (Button, { onClick: showRenew }, `Renew`),
			checkedIn && h (Button, { disabled: true }, `Checked in`),
			h (Button, { onClick: hostNote }, `\u{1F5D2}\u{FE0F}`),
		),
		h (RenewModal, { show: renewShow, onCancel: hideRenew, onSave: renewMembership }),
	)
})

const TextCell = ({ className = ``, children, ...props }) => {
	return h (Cell, { className: `Cell ${className}`, ...props },
		h (Text, null, children),
	)
}

const Cell = ({ className = ``, children, ...props }) => {
	return h (`span`, { className: `Cell ${className}`, ...props }, children)
}

const Button = ({ className = ``, onClick, children, ...props }) => {
	return h (`button`, { className: `Button ${className}`, onClick, ...props }, children)
}

const Text = ({ className = ``, children, ...props }) => {
	return h (`span`, { className: `Text ${className}`, ...props }, children)
}

const RenewModal = ({ show, onSave, onCancel }) => {
	const noteRef = useRef ()
	const typeMemberRef = useRef ()
	const typeTicketRef = useRef ()
	const typeGuestRef = useRef ()
	const onClick = useCallback (() => {
		let type = null
		if (typeMemberRef.current.checked) type = `member`
		if (typeTicketRef.current.checked) type = `ticket`
		if (typeGuestRef.current.checked) type = `guest`
		if (type === null) return
		const note = noteRef.current.value
		onSave ({ type, note })
	}, [ onSave, noteRef, typeMemberRef, typeTicketRef, typeGuestRef ])
	useEffect (() => { if (show) typeMemberRef.current.checked = true }, [ show ])
	return show && h (Modal, { onOverlay: onCancel },
		h (`h3`, null, `Renew membership`),
		h (`label`, null, h (`input`, { type: `radio`, name: `renewType`, ref: typeMemberRef }), `Membership`),
		h (`label`, null, h (`input`, { type: `radio`, name: `renewType`, ref: typeTicketRef }), `Ticket (Paid`),
		h (`label`, null, h (`input`, { type: `radio`, name: `renewType`, ref: typeGuestRef }), `Guest w/ Member`),
		h (`label`, null, h (`p`, null, `Note (optional)`), h (`input`, { ref: noteRef })),
		h (`p`, null,
			h (`button`, { onClick }, `Save and renew`),
		),
	)
}

const NewMemberModal = () => {
	const dispatch = useDispatch ()

	const name = useSelector (s => s.waiver.name)
	const phone = useSelector (s => s.waiver.phone)
	const note = useSelector (s => s.waiver.note)
	const plan = useSelector (s => s.waiver.plan)

	const downloadWaiver = useDownloadWaiver ()

	const save = useCallback (async () => {
		const person = uuid (5)
		dispatch ({ type: `APPEND`, sheet: `todo`, row: { date: timestampToday (), time: timestampNow (), person, name, phone, todo: `NEW PERSON ${plan}${note ? ` (${note})` : ``}` } })
		dispatch ({ type: `APPEND`, sheet: `checkins`, row: { person, date: timestampToday (), time: timestampNow (), note: plan === `member` ? `NEW` : `GUEST` } })

		await downloadWaiver ()

		dispatch ({ type: `WAIVER_SAVE` })
	}, [ name, phone, note, plan, dispatch, downloadWaiver ])

	const cancel = useCallback (() => dispatch ({ type: `WAIVER_ESCAPE` }), [ dispatch ])
	const setPlan = useCallback (ev => dispatch ({ type: `WAIVER_PLAN`, plan: ev.target.value }), [ dispatch ])
	const setNote = useCallback (ev => dispatch ({ type: `WAIVER_NOTE`, note: ev.target.value }), [ dispatch ])

	return h (Modal, { onClose: cancel },
		h (`h3`, null, `Member plan`),
		h (`label`, null, h (`input`, { type: `radio`, name: `plan`, onChange: setPlan, value: `member`, checked: plan === `member` }), `Membership`),
		h (`label`, null, h (`input`, { type: `radio`, name: `plan`, onChange: setPlan, value: `ticket`, checked: plan === `ticket` }), `Ticket (Paid)`),
		h (`label`, null, h (`input`, { type: `radio`, name: `plan`, onChange: setPlan, value: `freeTicket`, checked: plan === `freeTicket` }), `Ticket (Free)`),
		h (`label`, null, h (`input`, { type: `radio`, name: `plan`, onChange: setPlan, value: `guest`, checked: plan === `guest` }), `Guest w/ Member`),
		h (`label`, null, h (`p`, null, `Note (optional)`), h (`input`, { value: note, onChange: setNote })),
		h (`p`, null,
			h (`button`, { onClick: save }, `Save membership`),
		),
	)
}

const modalContainer = document.createElement (`div`)

document.addEventListener (`readystatechange`, () => {
	if (document.readyState === `interactive`) {
		document.body.appendChild (modalContainer)
	}
})

const Modal = ({ onClose, children }) => {
	useEffect (() => {
		const keyDown = ev => ev.keyCode === 27 && onClose ()
		window.addEventListener (`keydown`, keyDown)
		return () => window.removeEventListener (`keydown`, keyDown)
	}, [ onClose ])

	return createPortal (h (Fragment, null,
		h (`div`, { className: `ModalOverlay`, onClick: onClose }),
		h (`div`, { className: `Modal` }, children)
	), modalContainer)
}

const Waiver = () => {
	const dispatch = useDispatch ()

	const hasErrors = useSelector (s => !!s.waiver.pages.find (page => page.fields.find (field => field.validate && field.validate (s))))

	const length = useSelector (s => s.waiver.pages.length)
	const selectors = React.useMemo (() => Array (length).fill ().map ((_, i) => s => s.waiver.pages [i]), [ length ])
	const onSubmit = useCallback (() => dispatch ({ type: `WAIVER_SUBMIT` }), [ dispatch ])
	const onCancel = useCallback (() => dispatch ({ type: `WAIVER_CANCEL` }), [ dispatch ])

	const modal = useSelector (s => s.waiver.modal)

	return h (Fragment, null,
		h (`div`, { className: `Signing` },
			selectors.map ((selector, i) => h (Page, { key: i, selector })),
			h (`hr`),
			h (`div`, { style: { margin: `50px`, textAlign: `center` } },
				h (`button`, { onClick: onSubmit, disabled: hasErrors }, `Submit waiver`),
				h (`button`, { onClick: onCancel }, `Cancel`),
			),
		),
		modal && h (NewMemberModal),
	)
}

const useDownloadWaiver = () => {
	const state = useSelector (s => s)
	const pages = useSelector (s => s.waiver.pages)
	const name = useSelector (s => s.waiver.name)

	return useCallback (async () => {
		const pdf = await PDFLib.PDFDocument.create ()

		pages.forEach (() => pdf.addPage (PDFLib.PageSizes.Letter))

		await Promise.all (pages.map (async (page, i) => {
			const img = document.createElement (`img`)
			img.src = page.src
			if (!img.complete) await new Promise (res => img.addEventListener (`load`, res))
			const inch = img.width / 8.5

			const osc = document.createElement (`canvas`)
			osc.width = 8.5 * inch, osc.height = 11 * inch
			const ctx = osc.getContext (`2d`)

			ctx.drawImage (img, 0, 0, 8.5 * inch, 11 * inch)

			page.fields.forEach (({ signature, get, bottom, left, width, height }) => {
				if (signature) {
					ctx.drawImage (get (state), (left - 0.5 * width) * inch, (11 - bottom - height * 3) * inch, (width * 2.5) * inch, (height * 5) * inch)
				} else {
					ctx.font = `500 ${height * inch}px sans-serif`
					ctx.fillText (get (state), (left + 0.01) * inch, (11 - 0.035 - bottom) * inch)
				}
			})

			const png = await pdf.embedPng (osc.toDataURL (`image/png`))
			const pdfPage = pdf.getPages () [i]
			const size = pdfPage.getSize ()
			pdfPage.drawImage (png, { x: 0, y: 0, ...size })
		}))

		const bytes = await pdf.save ()
		const a = document.createElement (`a`)
		a.download = `${name} - Signed.pdf`
		a.href = `data:application/pdf;base64,${ByteBase64.bytesToBase64 (bytes)}`
		a.click ()
	}, [ state, pages, name ])
}

const Page = ({ selector }) => {
	const ref = useRef ()
	const src = useSelector (s => selector (s).src)
	const length = useSelector (s => selector (s).fields.length)
	const selectors = React.useMemo (() => Array (length).fill ().map ((_, i) => s => selector (s).fields [i]), [ length, selector ])

	const img = React.useMemo (() => {
		const img = document.createElement (`img`)
		img.src = src
		return img
	}, [ src ])

	useEffect (() => {
		ref.current.width = ref.current.clientWidth * window.devicePixelRatio
		ref.current.height = ref.current.clientHeight * window.devicePixelRatio
	}, [])

	const draw = React.useCallback (() => {
		ref.current.getContext (`2d`).drawImage (img, 0, 0, ref.current.width, ref.current.height)
	}, [ img ])

	useEffect (() => {
		img.complete ? draw () : img.addEventListener (`load`, draw)
	}, [ img, draw ])

	return h (`article`, null,
		h (`canvas`, { ref }),
		selectors.map ((selector, i) => h (Field, { key: i, selector })),
	)
}

const Field = ({ selector }) => {
	const { print, signature, ...props } = useSelector (selector)
	return  print ? null : signature ? h (SignatureField, props) : h (TextField, props)
}

const SignatureField = ({ set, bottom, left, width, height }) => {
	const dispatch = useDispatch ()
	const dispatchSet = React.useMemo (() => set && set (dispatch), [ set, dispatch ])
	const ref = useRef ()
	useEffect (() => {
		ref.current.width = ref.current.clientWidth * window.devicePixelRatio
		ref.current.height = ref.current.clientHeight * window.devicePixelRatio
		ref.current.getContext (`2d`).scale (window.devicePixelRatio, window.devicePixelRatio)
		const pad = new SignaturePad (ref.current, {
			dotSize: 2, minDistance: 1, throttle: 0 })
		dispatchSet (ref.current)
		return () => pad.clear ()
	}, [ set ])
	return h (`label`, { style: { bottom: `${bottom}in`, left: `${left}in`, width: `${width}in`, height: `${height}in` } },
		h (`input`, { disabled: true }),
		h (`canvas`, { ref, tabIndex: 0 }),
	)
}

const TextField = ({ readonly, phone, get, set, validate, bottom, left, width, height }) => {
	const dispatch = useDispatch ()
	const dispatchSet = React.useMemo (() => set && set (dispatch), [ set, dispatch ])

	const type = phone ? `phone` : `text`
	const onChange = React.useCallback (ev => dispatchSet && dispatchSet (ev.target.value), [ dispatchSet ])
	const value = useSelector (get)
	const error = useSelector (validate || (() => null))
	return h (`label`, { style: { bottom: `${bottom}in`, left: `${left}in`, width: `${width}in`, height: `${height}in`, font: `500 ${height}in sans-serif` } },
		h (`input`, { type, disabled: readonly, autoComplete: `off`, value, onChange }),
		error && h (`span`, { className: `warning` }, error)
	)
}

document.addEventListener (`readystatechange`, () => {
	if (document.readyState === `interactive`) {
		const wrapper = document.createElement (`div`)
		wrapper.setAttribute (`class`, `Wrapper`)
		render (h (Provider, { store }, h (Wrapper)), wrapper)
		document.body.appendChild (wrapper)
	}
})
